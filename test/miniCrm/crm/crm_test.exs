defmodule MiniCrm.CRMTest do
  use MiniCrm.DataCase

  alias MiniCrm.CRM

  describe "accounts" do
    alias MiniCrm.CRM.Account

    @valid_attrs %{address: "some address", assigned_to: "some assigned_to", category: "some category", city: "some city", comment: "some comment", country: "some country", email: "some email", name: "some name", phone: "some phone", postcode: "some postcode", rating: "some rating", s: "some s", tag: "some tag", website: "some website"}
    @update_attrs %{address: "some updated address", assigned_to: "some updated assigned_to", category: "some updated category", city: "some updated city", comment: "some updated comment", country: "some updated country", email: "some updated email", name: "some updated name", phone: "some updated phone", postcode: "some updated postcode", rating: "some updated rating", s: "some updated s", tag: "some updated tag", website: "some updated website"}
    @invalid_attrs %{address: nil, assigned_to: nil, category: nil, city: nil, comment: nil, country: nil, email: nil, name: nil, phone: nil, postcode: nil, rating: nil, s: nil, tag: nil, website: nil}

    def account_fixture(attrs \\ %{}) do
      {:ok, account} =
        attrs
        |> Enum.into(@valid_attrs)
        |> CRM.create_account()

      account
    end

    test "list_accounts/0 returns all accounts" do
      account = account_fixture()
      assert CRM.list_accounts() == [account]
    end

    test "get_account!/1 returns the account with given id" do
      account = account_fixture()
      assert CRM.get_account!(account.id) == account
    end

    test "create_account/1 with valid data creates a account" do
      assert {:ok, %Account{} = account} = CRM.create_account(@valid_attrs)
      assert account.address == "some address"
      assert account.assigned_to == "some assigned_to"
      assert account.category == "some category"
      assert account.city == "some city"
      assert account.comment == "some comment"
      assert account.country == "some country"
      assert account.email == "some email"
      assert account.name == "some name"
      assert account.phone == "some phone"
      assert account.postcode == "some postcode"
      assert account.rating == "some rating"
      assert account.s == "some s"
      assert account.tag == "some tag"
      assert account.website == "some website"
    end

    test "create_account/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CRM.create_account(@invalid_attrs)
    end

    test "update_account/2 with valid data updates the account" do
      account = account_fixture()
      assert {:ok, %Account{} = account} = CRM.update_account(account, @update_attrs)
      assert account.address == "some updated address"
      assert account.assigned_to == "some updated assigned_to"
      assert account.category == "some updated category"
      assert account.city == "some updated city"
      assert account.comment == "some updated comment"
      assert account.country == "some updated country"
      assert account.email == "some updated email"
      assert account.name == "some updated name"
      assert account.phone == "some updated phone"
      assert account.postcode == "some updated postcode"
      assert account.rating == "some updated rating"
      assert account.s == "some updated s"
      assert account.tag == "some updated tag"
      assert account.website == "some updated website"
    end

    test "update_account/2 with invalid data returns error changeset" do
      account = account_fixture()
      assert {:error, %Ecto.Changeset{}} = CRM.update_account(account, @invalid_attrs)
      assert account == CRM.get_account!(account.id)
    end

    test "delete_account/1 deletes the account" do
      account = account_fixture()
      assert {:ok, %Account{}} = CRM.delete_account(account)
      assert_raise Ecto.NoResultsError, fn -> CRM.get_account!(account.id) end
    end

    test "change_account/1 returns a account changeset" do
      account = account_fixture()
      assert %Ecto.Changeset{} = CRM.change_account(account)
    end
  end

  describe "opportunities" do
    alias MiniCrm.CRM.Opportunity

    @valid_attrs %{amount: "120.5", assigned_to: "some assigned_to", close_date: ~D[2010-04-17], comment: "some comment", discount: "120.5", name: "some name", probability: "120.5", stage: "some stage"}
    @update_attrs %{amount: "456.7", assigned_to: "some updated assigned_to", close_date: ~D[2011-05-18], comment: "some updated comment", discount: "456.7", name: "some updated name", probability: "456.7", stage: "some updated stage"}
    @invalid_attrs %{amount: nil, assigned_to: nil, close_date: nil, comment: nil, discount: nil, name: nil, probability: nil, stage: nil}

    def opportunity_fixture(attrs \\ %{}) do
      {:ok, opportunity} =
        attrs
        |> Enum.into(@valid_attrs)
        |> CRM.create_opportunity()

      opportunity
    end

    test "list_opportunities/0 returns all opportunities" do
      opportunity = opportunity_fixture()
      assert CRM.list_opportunities() == [opportunity]
    end

    test "get_opportunity!/1 returns the opportunity with given id" do
      opportunity = opportunity_fixture()
      assert CRM.get_opportunity!(opportunity.id) == opportunity
    end

    test "create_opportunity/1 with valid data creates a opportunity" do
      assert {:ok, %Opportunity{} = opportunity} = CRM.create_opportunity(@valid_attrs)
      assert opportunity.amount == Decimal.new("120.5")
      assert opportunity.assigned_to == "some assigned_to"
      assert opportunity.close_date == ~D[2010-04-17]
      assert opportunity.comment == "some comment"
      assert opportunity.discount == Decimal.new("120.5")
      assert opportunity.name == "some name"
      assert opportunity.probability == Decimal.new("120.5")
      assert opportunity.stage == "some stage"
    end

    test "create_opportunity/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CRM.create_opportunity(@invalid_attrs)
    end

    test "update_opportunity/2 with valid data updates the opportunity" do
      opportunity = opportunity_fixture()
      assert {:ok, %Opportunity{} = opportunity} = CRM.update_opportunity(opportunity, @update_attrs)
      assert opportunity.amount == Decimal.new("456.7")
      assert opportunity.assigned_to == "some updated assigned_to"
      assert opportunity.close_date == ~D[2011-05-18]
      assert opportunity.comment == "some updated comment"
      assert opportunity.discount == Decimal.new("456.7")
      assert opportunity.name == "some updated name"
      assert opportunity.probability == Decimal.new("456.7")
      assert opportunity.stage == "some updated stage"
    end

    test "update_opportunity/2 with invalid data returns error changeset" do
      opportunity = opportunity_fixture()
      assert {:error, %Ecto.Changeset{}} = CRM.update_opportunity(opportunity, @invalid_attrs)
      assert opportunity == CRM.get_opportunity!(opportunity.id)
    end

    test "delete_opportunity/1 deletes the opportunity" do
      opportunity = opportunity_fixture()
      assert {:ok, %Opportunity{}} = CRM.delete_opportunity(opportunity)
      assert_raise Ecto.NoResultsError, fn -> CRM.get_opportunity!(opportunity.id) end
    end

    test "change_opportunity/1 returns a opportunity changeset" do
      opportunity = opportunity_fixture()
      assert %Ecto.Changeset{} = CRM.change_opportunity(opportunity)
    end
  end

  describe "leads" do
    alias MiniCrm.Crm.Lead

    @valid_attrs %{city: "some city", company: "some company", country: "some country", department: "some department", email: "some email", name: "some name", phone: "some phone", status: "some status", street: "some street", title: "some title"}
    @update_attrs %{city: "some updated city", company: "some updated company", country: "some updated country", department: "some updated department", email: "some updated email", name: "some updated name", phone: "some updated phone", status: "some updated status", street: "some updated street", title: "some updated title"}
    @invalid_attrs %{city: nil, company: nil, country: nil, department: nil, email: nil, name: nil, phone: nil, status: nil, street: nil, title: nil}

    def lead_fixture(attrs \\ %{}) do
      {:ok, lead} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Crm.create_lead()

      lead
    end

    test "list_leads/0 returns all leads" do
      lead = lead_fixture()
      assert Crm.list_leads() == [lead]
    end

    test "get_lead!/1 returns the lead with given id" do
      lead = lead_fixture()
      assert Crm.get_lead!(lead.id) == lead
    end

    test "create_lead/1 with valid data creates a lead" do
      assert {:ok, %Lead{} = lead} = Crm.create_lead(@valid_attrs)
      assert lead.city == "some city"
      assert lead.company == "some company"
      assert lead.country == "some country"
      assert lead.department == "some department"
      assert lead.email == "some email"
      assert lead.name == "some name"
      assert lead.phone == "some phone"
      assert lead.status == "some status"
      assert lead.street == "some street"
      assert lead.title == "some title"
    end

    test "create_lead/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Crm.create_lead(@invalid_attrs)
    end

    test "update_lead/2 with valid data updates the lead" do
      lead = lead_fixture()
      assert {:ok, %Lead{} = lead} = Crm.update_lead(lead, @update_attrs)
      assert lead.city == "some updated city"
      assert lead.company == "some updated company"
      assert lead.country == "some updated country"
      assert lead.department == "some updated department"
      assert lead.email == "some updated email"
      assert lead.name == "some updated name"
      assert lead.phone == "some updated phone"
      assert lead.status == "some updated status"
      assert lead.street == "some updated street"
      assert lead.title == "some updated title"
    end

    test "update_lead/2 with invalid data returns error changeset" do
      lead = lead_fixture()
      assert {:error, %Ecto.Changeset{}} = Crm.update_lead(lead, @invalid_attrs)
      assert lead == Crm.get_lead!(lead.id)
    end

    test "delete_lead/1 deletes the lead" do
      lead = lead_fixture()
      assert {:ok, %Lead{}} = Crm.delete_lead(lead)
      assert_raise Ecto.NoResultsError, fn -> Crm.get_lead!(lead.id) end
    end

    test "change_lead/1 returns a lead changeset" do
      lead = lead_fixture()
      assert %Ecto.Changeset{} = Crm.change_lead(lead)
    end
  end

  describe "contacts" do
    alias MiniCrm.Crm.Contact

    @valid_attrs %{city: "some city", country: "some country", department: "some department", email: "some email", name: "some name", phone: "some phone", street: "some street", title: "some title"}
    @update_attrs %{city: "some updated city", country: "some updated country", department: "some updated department", email: "some updated email", name: "some updated name", phone: "some updated phone", street: "some updated street", title: "some updated title"}
    @invalid_attrs %{city: nil, country: nil, department: nil, email: nil, name: nil, phone: nil, street: nil, title: nil}

    def contact_fixture(attrs \\ %{}) do
      {:ok, contact} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Crm.create_contact()

      contact
    end

    test "list_contacts/0 returns all contacts" do
      contact = contact_fixture()
      assert Crm.list_contacts() == [contact]
    end

    test "get_contact!/1 returns the contact with given id" do
      contact = contact_fixture()
      assert Crm.get_contact!(contact.id) == contact
    end

    test "create_contact/1 with valid data creates a contact" do
      assert {:ok, %Contact{} = contact} = Crm.create_contact(@valid_attrs)
      assert contact.city == "some city"
      assert contact.country == "some country"
      assert contact.department == "some department"
      assert contact.email == "some email"
      assert contact.name == "some name"
      assert contact.phone == "some phone"
      assert contact.street == "some street"
      assert contact.title == "some title"
    end

    test "create_contact/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Crm.create_contact(@invalid_attrs)
    end

    test "update_contact/2 with valid data updates the contact" do
      contact = contact_fixture()
      assert {:ok, %Contact{} = contact} = Crm.update_contact(contact, @update_attrs)
      assert contact.city == "some updated city"
      assert contact.country == "some updated country"
      assert contact.department == "some updated department"
      assert contact.email == "some updated email"
      assert contact.name == "some updated name"
      assert contact.phone == "some updated phone"
      assert contact.street == "some updated street"
      assert contact.title == "some updated title"
    end

    test "update_contact/2 with invalid data returns error changeset" do
      contact = contact_fixture()
      assert {:error, %Ecto.Changeset{}} = Crm.update_contact(contact, @invalid_attrs)
      assert contact == Crm.get_contact!(contact.id)
    end

    test "delete_contact/1 deletes the contact" do
      contact = contact_fixture()
      assert {:ok, %Contact{}} = Crm.delete_contact(contact)
      assert_raise Ecto.NoResultsError, fn -> Crm.get_contact!(contact.id) end
    end

    test "change_contact/1 returns a contact changeset" do
      contact = contact_fixture()
      assert %Ecto.Changeset{} = Crm.change_contact(contact)
    end
  end
end
