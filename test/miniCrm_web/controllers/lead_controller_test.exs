defmodule MiniCrmWeb.LeadControllerTest do
  use MiniCrmWeb.ConnCase

  alias MiniCrm.Crm

  @create_attrs %{city: "some city", company: "some company", country: "some country", department: "some department", email: "some email", name: "some name", phone: "some phone", status: "some status", street: "some street", title: "some title"}
  @update_attrs %{city: "some updated city", company: "some updated company", country: "some updated country", department: "some updated department", email: "some updated email", name: "some updated name", phone: "some updated phone", status: "some updated status", street: "some updated street", title: "some updated title"}
  @invalid_attrs %{city: nil, company: nil, country: nil, department: nil, email: nil, name: nil, phone: nil, status: nil, street: nil, title: nil}

  def fixture(:lead) do
    {:ok, lead} = Crm.create_lead(@create_attrs)
    lead
  end

  describe "index" do
    test "lists all leads", %{conn: conn} do
      conn = get(conn, Routes.lead_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Leads"
    end
  end

  describe "new lead" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.lead_path(conn, :new))
      assert html_response(conn, 200) =~ "New Lead"
    end
  end

  describe "create lead" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.lead_path(conn, :create), lead: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.lead_path(conn, :show, id)

      conn = get(conn, Routes.lead_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Lead"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.lead_path(conn, :create), lead: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Lead"
    end
  end

  describe "edit lead" do
    setup [:create_lead]

    test "renders form for editing chosen lead", %{conn: conn, lead: lead} do
      conn = get(conn, Routes.lead_path(conn, :edit, lead))
      assert html_response(conn, 200) =~ "Edit Lead"
    end
  end

  describe "update lead" do
    setup [:create_lead]

    test "redirects when data is valid", %{conn: conn, lead: lead} do
      conn = put(conn, Routes.lead_path(conn, :update, lead), lead: @update_attrs)
      assert redirected_to(conn) == Routes.lead_path(conn, :show, lead)

      conn = get(conn, Routes.lead_path(conn, :show, lead))
      assert html_response(conn, 200) =~ "some updated city"
    end

    test "renders errors when data is invalid", %{conn: conn, lead: lead} do
      conn = put(conn, Routes.lead_path(conn, :update, lead), lead: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Lead"
    end
  end

  describe "delete lead" do
    setup [:create_lead]

    test "deletes chosen lead", %{conn: conn, lead: lead} do
      conn = delete(conn, Routes.lead_path(conn, :delete, lead))
      assert redirected_to(conn) == Routes.lead_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.lead_path(conn, :show, lead))
      end
    end
  end

  defp create_lead(_) do
    lead = fixture(:lead)
    {:ok, lead: lead}
  end
end
