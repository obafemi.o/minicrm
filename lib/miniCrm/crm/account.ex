defmodule MiniCrm.CRM.Account do
  use Ecto.Schema
  import Ecto.Changeset

  schema "accounts" do
    field :address, :string
    field :assigned_to, :string
    field :category, :string
    field :city, :string
    field :comment, :string
    field :country, :string
    field :email, :string
    field :name, :string
    field :phone, :string
    field :postcode, :string
    field :rating, :string
    field :tag, :string
    field :website, :string
    has_many :opportunities, MiniCrm.CRM.Opportunity
    has_many :contacts, MiniCrm.CRM.Contact

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:name, :assigned_to, :category, :rating, :tag, :comment, :phone, :website, :email, :address, :city, :country, :postcode])
    |> validate_required([:name, :assigned_to, :category, :rating, :tag, :comment, :phone, :website, :email, :address, :city, :country, :postcode])
  end
end
