defmodule MiniCrm.CRM.Contact do
  use Ecto.Schema
  import Ecto.Changeset

  alias MiniCrm.CRM.Account

  schema "contacts" do
    field :city, :string
    field :country, :string
    field :department, :string
    field :email, :string
    field :name, :string
    field :phone, :string
    field :street, :string
    field :title, :string
    belongs_to :account, Account

    timestamps()
  end

  @doc false
  def changeset(contact, attrs) do
    contact
    |> cast(attrs, [:name, :email, :phone, :title, :department, :street, :city, :country])
    |> validate_required([:name, :email, :phone, :title, :department, :street, :city, :country])
  end
end
