defmodule MiniCrm.CRM.Lead do
  use Ecto.Schema
  import Ecto.Changeset

  schema "leads" do
    field :city, :string
    field :company, :string
    field :country, :string
    field :department, :string
    field :email, :string
    field :name, :string
    field :phone, :string
    field :status, :string
    field :street, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(lead, attrs) do
    lead
    |> cast(attrs, [:name, :department, :email, :phone, :status, :title, :company, :street, :city, :country])
    |> validate_required([:name, :department, :email, :phone, :title, :company, :street, :city, :country])
  end
end
