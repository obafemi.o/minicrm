defmodule MiniCrmWeb.Router do
  use MiniCrmWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MiniCrmWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/accounts", AccountController
    resources "/opportunities", OpportunityController
    resources "/leads", LeadController
    resources "/contacts", ContactController
  end

  # Other scopes may use custom stacks.
  # scope "/api", MiniCrmWeb do
  #   pipe_through :api
  # end
end
