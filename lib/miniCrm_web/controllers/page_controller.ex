defmodule MiniCrmWeb.PageController do
  use MiniCrmWeb, :controller

  alias MiniCrm.CRM
  alias MiniCrm.CRM.Account

  def index(conn, _params) do
    accounts = CRM.list_accounts()
    render(conn, "index.html", accounts: accounts)
  end
  # def index(conn, _params) do
  #   render(conn, "index.html")
  # end
end
