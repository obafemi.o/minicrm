defmodule MiniCrm.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :name, :string
      add :assigned_to, :string
      add :category, :string
      add :rating, :string
      add :tag, :string
      add :comment, :text
      add :phone, :string
      add :website, :string
      add :email, :string
      add :address, :string
      add :city, :string
      add :country, :string
      add :postcode, :string

      timestamps()
    end

  end
end
